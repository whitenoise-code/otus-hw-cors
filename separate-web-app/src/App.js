import React, { useEffect, useState } from "react";
import logo from './logo.svg';
import './App.css';
import FetchData from "./FetchData";

function App() {

  const [values, setValues] = useState({forecasts: [], loading: true})

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/weatherforecast`)
        .then(response => {
          response.json().then(data => {
            setValues({forecasts: data, loading: false})
          })
        })
  }, [])

  return (
      <div className="App">
        <header className="App-header">
          <FetchData data={values}/>
        </header>

      </div>
  );
}

export default App;
